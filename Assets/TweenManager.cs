﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class TweenManager : MonoBehaviour 
{
    public void FadeIn(Text text) {
        text.rectTransform.localScale = new Vector3(1f, 0.1f, 1f);

        text.DOFade(1f, 1f).SetEase(Ease.InOutSine);
        text.rectTransform.DOMoveY(text.rectTransform.position.y + 25, 0.75f).SetEase(Ease.OutBack);
        text.rectTransform.DOScale(new Vector3(1f, 1f, 1f), 0.75f).SetEase(Ease.OutBack);
    }

    public void FadeOut(Text text) {
        TweenCallback cb = new TweenCallback(() => {
            Destroy(text.gameObject);
        });

        float time = Random.Range(1f, 1.75f);
        text.DOFade(0f, time * 0.9f).SetEase(Ease.InOutSine);
        text.rectTransform.DOMoveY(text.rectTransform.position.y + Random.Range(75, 100), time).SetEase(Ease.InSine).OnComplete(cb);
        text.rectTransform.DOScale(new Vector3(2f, 2f, 0), time);
        text.rectTransform.DORotate(new Vector3(0, 0, Random.Range(-25f, 25f)), time);
    }
}
