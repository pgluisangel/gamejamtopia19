﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Story", menuName = "Scriptables/Story", order = 0)]
public class Story : ScriptableObject {
    public int sceneId;
    public bool isFirstTime = true;
    public Fragment[] firstFragments;
    public Fragment[] secondfragments;
}
