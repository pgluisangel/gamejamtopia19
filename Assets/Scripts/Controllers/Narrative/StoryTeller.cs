﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;
using System;

public class StoryTeller : MonoBehaviour {

    public GameObject textModel;
    public Canvas canvas;
    public Story[] stories;
    public enum States 
    {
        Idle,
        Setting,
        Speaking,
        DangerWord,
        Options,
        CheckOptions,
        Defeat
    }
    public States state;
    public Story currentStory;
    public Fragment currentFragment;
    public string[] currentPassage;
    public List<char> currentPlayerWord;
    public List<Text> textsOnScreen;
    public List<Text> dangerTexts;
    public List<String> originalDangerTexts;

    public List<List<char>> playerOptions;

    public int fragmentIndex = -1;
    public int passageIndex = 0;
    public int playerWordIndex = 0;
    public char playerInput;
    public float lastWordWidth = 0;
    public float lastWordPos = 0;

    public TweenManager tween;
    public ChangeSceneManager csm;
    public GameDirector gd;

    private bool wordsFlag = true;
    private bool defeat = false;
    private float easyTimer;
    private int optionInd;
    private bool flagDanger = false;
    private bool flagOptions = false;
    private string checkText;

    void Start() {
        for (int i = 0; i < stories.Length; i++) {
            stories[i].isFirstTime = true;
        }
        state = States.Idle;
        StartCoroutine("FSM");
    }

    IEnumerator Idle() {
        yield return 0;
    }

    IEnumerator FSM() {
        while (true) { yield return StartCoroutine(state.ToString()); }
    }

    IEnumerator Setting() {
        currentStory = getStory();
        if (!currentStory) yield break;
        fragmentIndex = -1;
        playerWordIndex = 0;
        // currentFragment = this.getFragment();
        // currentPassage = this.getPassage();
        currentPlayerWord.Clear();
        resetForPassage();
        state = States.Speaking;
        yield return 0;
    }

    IEnumerator Speaking() {
        while (state == States.Speaking) {
            // Iterate throught passages and act according
            if (defeat) {
                passageIndex = 0;
                while (passageIndex < currentPassage.Length) {
                    string passage = currentPassage[passageIndex];
                    bool isTyped = passage.Contains("%");
                    if (isTyped) {
                        yield return StartCoroutine("DangerWord");
                    } else {
                        StartCoroutine(spawnText(passage, isTyped, passageIndex));
                        passageIndex++;
                        yield return new WaitForSeconds(0.10f);
                    }
                }

                state = States.Idle;
                yield return new WaitForSeconds(1.5f);
                currentStory.isFirstTime = false;
                csm.ChangeScene(currentFragment.failOption.sceneName);
                yield break;
            } else {
                while (currentFragment = getFragment()) {
                    currentPassage = getPassage();
                    while (passageIndex < currentPassage.Length) {
                        string passage = currentPassage[passageIndex];
                        bool isTyped = passage.Contains("%");
                        if (isTyped) {
                            yield return StartCoroutine("DangerWord");
                        } else {
                            StartCoroutine(spawnText(passage, isTyped, passageIndex));
                            passageIndex++;
                            yield return new WaitForSeconds(0.10f);
                        }
                    }
                    if (currentFragment.options.Length > 0) yield return StartCoroutine("Options");
                    yield return new WaitForSeconds(1.5f);
                    StartCoroutine(fadeOutWords());
                    yield return new WaitForSeconds(0.75f);
                }
            }

            // state = States.Idle;
        }
    }

    IEnumerator DangerWord() {
        flagDanger = true;
        dangerTexts.Clear();
        originalDangerTexts.Clear();

        while (passageIndex < currentPassage.Length && currentPassage[passageIndex].Contains("%")) {
            StartCoroutine(spawnText(splicePlayerWord(currentPassage[passageIndex]), true, passageIndex));
            passageIndex++;
            yield return new WaitForSeconds(0.10f);
        }

        // TODO timer
        playerWordIndex = 0;
        wordsFlag = false;

        // easyTimer = 10.0f;
        while (!wordsFlag) {
            yield return new WaitForSeconds(1);
            // easyTimer -= 1;
            // if (easyTimer <= 0) {
            //     state = States.Defeat;
            //     wordsFlag = true;
            //     currentPlayerWord.Clear();
            //     yield break;
            // }
        }
        currentPlayerWord.Clear();
        flagDanger = false;
        state = States.Speaking;
    }

    IEnumerator Options() {
        dangerTexts.Clear();
        originalDangerTexts.Clear();
        float initPos = lastWordPos;
        float initWidth = lastWordWidth;
        playerOptions = new List<List<char>>();
        for (int i = 0; i < currentFragment.options.Length; i++) {
            lastWordPos = initPos;
            lastWordWidth = initWidth;
            String passage = currentFragment.options[i].passage;
            String[] texts = passage.Split(' ');

            for (int n = 0; n < texts.Length; n++) {
                if (texts[n].Contains("%")) {
                    StartCoroutine(spawnOptions(splicePlayerOption(texts[n]), true, passageIndex, i));
                    passageIndex--;
                } else {
                    StartCoroutine(spawnOptions(texts[n], false, passageIndex, i));
                }
                yield return new WaitForSeconds(0.05f);
            }
        }
        yield return StartCoroutine("CheckOptions");
    }

    IEnumerator CheckOptions() {
        playerWordIndex = 0;
        wordsFlag = false;
        flagOptions = true;        

        // easyTimer = 10.0f;
        while(!wordsFlag) {
            yield return new WaitForSeconds(1);
            // easyTimer -= 1;
            // if (easyTimer <= 0) {
            //     // state = States.Defeat;
            //     wordsFlag = true;
            //     currentPlayerWord.Clear();
            //     yield return StartCoroutine("Defeat");
            //     // yield break;
            // }
        }

        if (!defeat) {
            StartCoroutine(fadeOutWords());
            yield return new WaitForSeconds(2.0f);
            
            playerWordIndex = 0;
            state = States.Idle;

            currentStory.isFirstTime = false;
            csm.ChangeScene(currentFragment.options[optionInd].sceneName);
            flagOptions = false;
            yield break;
        }
        yield return 0;
    }

    IEnumerator Defeat() {
        StartCoroutine(fadeOutWords());
        yield return new WaitForSeconds(2.0f);
        defeat = true;
        
        currentPassage = currentFragment.failOption.passage.Split(' ');
        resetForPassage();
        state = States.Speaking;

        yield return StartCoroutine("Speaking");
    }

    void resetForPassage() {
        currentPlayerWord.Clear();
        passageIndex = 0;
        lastWordWidth = 0;
        lastWordPos = textModel.transform.position.x;
    }

    void Update() {
        if (Time.timeScale == 0.0f) return;
        if (flagDanger && !wordsFlag) {
            playerInput = '\0';
            foreach(char c in Input.inputString) {
                playerInput = c;
            }
            if (playerInput != '\0') {
                if (checkInput()) {
                    if (playerWordIndex < currentPlayerWord.Count - 1) correctKey();
                    else {
                        correctKey();
                        correctWord();
                        if(checkText == "subirlas") gd.subir();
                        if(checkText == "Colocartela") gd.maskOn();
                        if(checkText == "coloques") gd.maskOn();
                        if(checkText == "observas") gd.redDeath();
                        if(checkText == "tranquilizar") gd.relax();
                    }
                }
            }
        }

        if (flagOptions && !wordsFlag) {
            playerInput = '\0';
            foreach(char c in Input.inputString) {
                playerInput = c;
            }
            if (playerInput != '\0') {
                if (checkOption()) {
                    if (playerWordIndex < originalDangerTexts[optionInd].Length - 1) correctOption();
                    else {
                        correctOption();
                        correctWord();
                        if(checkText == "subirlas") gd.subir();
                        if(checkText == "Colocartela") gd.maskOn();
                        if(checkText == "coloques") gd.maskOn();
                        if(checkText == "observas") gd.redDeath();
                        if(checkText == "tranquilizar") gd.relax();
                    }
                }
            }
        }
    }

    bool checkOption() {
        optionInd = -1;
        for (int i = 0; i < playerOptions.Count; i++) {
            if (playerWordIndex < playerOptions[i].Count) {
                if (playerOptions[i][playerWordIndex] == playerInput) {
                    optionInd = i;
                    return true;
                }
            }
        }
        return false;
    }

    void correctOption() {
        int totalLength = 0;
        int charInd = -1;
        int dangerWordInd = -1;
        for (int n = 0; n < originalDangerTexts.Count; n++) {
            charInd = playerWordIndex;
            totalLength += originalDangerTexts[n].Length;
            if (playerWordIndex < totalLength) {
                dangerWordInd = n;
                n = originalDangerTexts.Count;
            }
        }
        if(dangerWordInd != -1) optionToGreen(optionInd, charInd);
        playerWordIndex++;
    }

    void correctKey() {
        int totalLength = 0;
        int charInd = 0;
        int dangerWordInd = 0;
        for (int i = 0; i < originalDangerTexts.Count; i++) {
            charInd = playerWordIndex - totalLength;
            totalLength += originalDangerTexts[i].Length;
            if (playerWordIndex < totalLength) {
                dangerWordInd = i;
                i = originalDangerTexts.Count;
            }
        }
        charToGreen(dangerWordInd, charInd);

        playerWordIndex++;
    }

    void optionToGreen(int optionInd, int wordInd) {
        Text text = dangerTexts[optionInd];
        String origText = originalDangerTexts[optionInd];
        String newString = "<color=#00FF00>";
        for (int i = 0; i < wordInd + 1; i++) {
            newString += origText[i].ToString();
        }
        
        newString += "</color>";
        text.text = origText.Remove(0, wordInd + 1);
        text.text = text.text.Insert(0, newString);

        checkText = origText;

        gd.sfx(0);
    }

    void charToGreen(int dangerInd, int wordInd) {
        Text text = dangerTexts[dangerInd];
        String origText = originalDangerTexts[dangerInd];
        String newString = "<color=#00FF00>";
        for (int i = 0; i < wordInd + 1; i++) {
            newString += origText[i].ToString();
        }
        newString += "</color>";
        text.text = origText.Remove(0, wordInd + 1);
        text.text = text.text.Insert(0, newString);
        
        checkText = origText;

        gd.sfx(0);
    }

    bool checkInput() {
        return playerInput == currentPlayerWord[playerWordIndex];
    }

    private Story getStory() {
        int sceneId = SceneManager.GetActiveScene().buildIndex;
        for (int i = 0; i < stories.Length; i++) {
            if (stories[i].sceneId == sceneId) return stories[i];
        }
        state = States.Idle;
        return null;
    }

    private Fragment getFragment() {
        if (currentStory == null) return null;
        fragmentIndex++;
        if (currentStory.isFirstTime) {
            if (fragmentIndex < currentStory.firstFragments.Length) return currentStory.firstFragments[fragmentIndex];
        } else {
            if (fragmentIndex < currentStory.secondfragments.Length) return currentStory.secondfragments[fragmentIndex];
        }
        return null;
    }

    string splicePlayerWord(string word) {
        word = word.Substring(1, word.Length - 2);
        for (int i = 0; i < word.Length; i++) {
            currentPlayerWord.Add(word[i]);
        }
        return word;
    }

    string splicePlayerOption(string word) {
        List<char> option = new List<char>();
        word = word.Substring(1, word.Length - 2);
        for (int i = 0; i < word.Length; i++) {
            currentPlayerWord.Add(word[i]);
            option.Add(word[i]);
        }
        playerOptions.Add(option);
        return word;
    }

    private string[] getPassage() {
        passageIndex = 0;
        resetForPassage();
        return currentFragment.passage.Split(' ');
    }

    private void correctWord() {
        wordsFlag = true;
    }

    IEnumerator fadeOutWords() {
        List<Text> textsToFade = new List<Text>(textsOnScreen);
        textsOnScreen.Clear();
        foreach(Text text in textsToFade) {
            tween.FadeOut(text);
            yield return new WaitForSeconds(0.1f);
        }
    }

    IEnumerator spawnText(string text, bool isTyped, int index) {
        GameObject copiedText = Instantiate(textModel, canvas.transform);
        Text copyText = copiedText.GetComponent<Text>();
        copyText.text = text;

        if (isTyped) {
            copyText.color = new Color(255, 0, 0, 255);
            dangerTexts.Add(copyText);
            originalDangerTexts.Add(copyText.text);
        } else copyText.color = new Color(0, 0, 0, 255);
        
        copyText.transform.position = new Vector3(
            lastWordPos + lastWordWidth,
            copyText.transform.position.y,
            copyText.transform.position.z
        );

        tween.FadeIn(copyText);
        textsOnScreen.Add(copyText);

        yield return 0;

        lastWordWidth = copyText.rectTransform.rect.width + 10;
        lastWordPos = copyText.transform.position.x;
    }

    IEnumerator spawnOptions(string text, bool isTyped, int index, int optIndex) {
        GameObject copiedText = Instantiate(textModel, canvas.transform);
        Text copyText = copiedText.GetComponent<Text>();
        copyText.text = text;
        if (isTyped) {
            copyText.color = new Color(255, 0, 0, 255);
            dangerTexts.Add(copyText);
            originalDangerTexts.Add(copyText.text);
        } else copyText.color = new Color(0, 0, 0, 255);
        
        copyText.transform.position = new Vector3(
            lastWordPos + lastWordWidth,
            copyText.transform.position.y - ((copyText.rectTransform.rect.height - 10) * optIndex),
            copyText.transform.position.z
        );

        tween.FadeIn(copyText);
        textsOnScreen.Add(copyText);

        yield return 0;

        lastWordWidth = copyText.rectTransform.rect.width + 10;
        lastWordPos = copyText.transform.position.x;
    }
}
