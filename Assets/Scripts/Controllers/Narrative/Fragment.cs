﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Story", menuName = "Scriptables/Fragment", order = 1)]
public class Fragment : ScriptableObject {
    public string passage;
    public Option[] options;
    public Option failOption;
}
