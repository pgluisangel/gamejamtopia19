﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPause : MonoBehaviour {

    public void TogglePause() {
        if (Time.timeScale == 1.0f) {
            Time.timeScale = 0.0f;
        }
        else {
            Time.timeScale = 1.0f;
        }
    }
}
