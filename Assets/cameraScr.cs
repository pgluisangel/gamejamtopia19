﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraScr : MonoBehaviour
{
    public Canvas canvas;
    void OnLevelWasLoaded(int scene) {
        canvas.worldCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }
}
