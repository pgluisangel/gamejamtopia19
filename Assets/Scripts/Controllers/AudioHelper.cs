﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Audio;

public class AudioHelper : MonoBehaviour
{

    public AudioMixer mixer;


    //public AudioClip Music_Base;
    public AudioClip[] AC_Music_Lvls;
    public AudioClip AC_Music_Menu;
    public AudioMixerGroup[] MixerMusicGroups;

    private AudioSource AS_Base;
    private AudioSource AS_MenuMusic;
    public AudioSource[] AS_Musics;
    private AudioSource AS_Limb;
    private AudioSource AS_Limb_loop;

    public float lowPitchRange = .85f;              //The lowest a sound effect will be randomly pitched.
    public float highPitchRange = 1.15f;            //The highest a sound effect will be randomly pitched.

    [Header("Game sounds")]
    public AudioClip sfx_Limb_Pick;
    public AudioClip sfx_Limb_Drop;
    public AudioClip sfx_Limb_Loop;
    public AudioClip sfx_PoseOK;
    public AudioClip sfx_PoseKO;
    public AudioClip sfx_GameOver;
    public AudioClip sfx_PinataHit;
    public AudioClip sfx_Nirvana;
    public AudioClip ui_confirm;
    public AudioClip ui_select;
    public AudioClip ui_something;
    public AudioClip sfx_levelDown;
    public AudioClip sfx_levelUp;
    public AudioClip sfx_sitarHit;


    [Header("Control ME")]
    [Range(0, 100)]
    public float YogaPower = 0;
    public float TransitionTimer = 2.0f;

    private void Awake()
    {
        if (AS_Base == null)
        {
            AS_Base = this.GetComponent<AudioSource>();
        }
        //Instantiate group
        AS_MenuMusic = this.gameObject.AddComponent<AudioSource>();
        AS_MenuMusic.outputAudioMixerGroup = mixer.FindMatchingGroups("Music")[0];
        AS_MenuMusic.clip = AC_Music_Menu;
        AS_MenuMusic.loop = true;

    }
    // Use this for initialization
    void Start()
    {
        AS_Musics = new AudioSource[AC_Music_Lvls.Length];
        for (int i = 0; i < AC_Music_Lvls.Length; i++)
        {
            //AudioClip acc = Music_Lvls[0];
            AS_Musics[i] = this.gameObject.AddComponent<AudioSource>();
            AS_Musics[i].outputAudioMixerGroup = MixerMusicGroups[i];
            AS_Musics[i].clip = AC_Music_Lvls[i];
            AS_Musics[i].loop = true;
        }

      
        
        AS_Limb = this.gameObject.AddComponent<AudioSource>();
        AS_Limb.outputAudioMixerGroup = mixer.FindMatchingGroups("SFX")[0];

        AS_Limb_loop = this.gameObject.AddComponent<AudioSource>();
        AS_Limb.loop = true;
        AS_Limb_loop.outputAudioMixerGroup = mixer.FindMatchingGroups("SFX")[0];
        //PlayMenMusic();
    }

    private int _curYogaPower = 0;
    // Update is called once per frame
    void Update()
    {
        if(YogaPower <= 20 && _curYogaPower != 0)
        {
            mixer.FindSnapshot("YogaMusic_Base").TransitionTo(TransitionTimer);
            _curYogaPower = 0;
            AudioSource.PlayClipAtPoint(sfx_sitarHit, this.transform.position, 0.5f);

        }
        else if(YogaPower > 20 && YogaPower <=40 && _curYogaPower != 1)
        {
            mixer.FindSnapshot("YogaMusic_Lvl0").TransitionTo(TransitionTimer);
            _curYogaPower = 1;
            AudioSource.PlayClipAtPoint(sfx_sitarHit, this.transform.position, 0.5f);

        }
        else if (YogaPower > 40 && YogaPower <= 60 && _curYogaPower != 2)
        {
            mixer.FindSnapshot("YogaMusic_Lvl1").TransitionTo(TransitionTimer);
            _curYogaPower = 2;
            AudioSource.PlayClipAtPoint(sfx_sitarHit, this.transform.position, 0.5f);
        }
        else if (YogaPower > 60 && YogaPower <= 80 && _curYogaPower != 3)
        {
            mixer.FindSnapshot("YogaMusic_Lvl2").TransitionTo(TransitionTimer);
            _curYogaPower = 3;
            AudioSource.PlayClipAtPoint(sfx_sitarHit, this.transform.position, 0.5f);
        }
        else if (YogaPower > 80 && YogaPower <= 100 && _curYogaPower != 4)
        {
            mixer.FindSnapshot("YogaMusic_Lvl3").TransitionTo(TransitionTimer);
            _curYogaPower = 4;
            AudioSource.PlayClipAtPoint(sfx_sitarHit, this.transform.position, 0.5f);
        }

        if (_limbPicked)
        {
            if(!AS_Limb_loop.isPlaying)
            {
                AS_Limb_loop.clip = sfx_Limb_Loop;
                AS_Limb_loop.loop = true;
                AS_Limb_loop.PlayDelayed(0.5f);
                AS_Limb_loop.time = Random.Range(0, AS_Limb_loop.time);
            }
            //Mathf.Lerp(AS_Limb_loop.volume, 1, Time.deltaTime * 10);
        }
        else
        {
            AS_Limb_loop.clip = null;
            //AS_Limb_loop.volume = 0;
        }

#if DEBUG

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            YogaPower = 0;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            YogaPower = 30;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            YogaPower = 50;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            YogaPower = 70;
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            YogaPower = 90;
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            PlayGameMusic();
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            PlayMenMusic();
                //Play_LimbPick();
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            Play_LimbDrop();
        }
#endif
    }

    public void PlayGameMusic()
    {
        mixer.FindSnapshot("YogaMusic_Base").TransitionTo(TransitionTimer);
        foreach (AudioSource am in AS_Musics)
        {
            am.Play();
        }
        if(AS_MenuMusic != null && AS_MenuMusic.isPlaying)
            AS_MenuMusic.Stop();
    }
    public void PlayMenMusic()
    {
        mixer.FindSnapshot("Menu").TransitionTo(TransitionTimer);
        AS_MenuMusic.Play();

        foreach (AudioSource am in AS_Musics)
        {
            am.Stop();
        }
    }

    public void PlaySingle(AudioClip clip, AudioSource source)
    {
        if (!source.isPlaying)
        {
            source.PlayOneShot(clip);
        }
    }

    public void PlayRandom(AudioClip[] clips, AudioSource source)
    {
        int randomIndex = Random.Range(0, clips.Length);
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);

        if (!source.isPlaying)
        {
            source.pitch = randomPitch;
            source.PlayOneShot(clips[randomIndex]);
        }
    }

    #region sound playa
    private bool _limbPicked = false;
    public void Play_LimbPick()
    {
        PlaySingle(sfx_Limb_Pick, AS_Limb);
        _limbPicked = true;
    }
    public void Play_LimbDrop()
    {
        PlaySingle(sfx_Limb_Drop, AS_Limb);
        _limbPicked = false;
    }

    public void Play_PoseOK() { PlaySingle(sfx_PoseOK, AS_Base); }
    public void Play_PoseKO() { PlaySingle(sfx_PoseKO, AS_Base); }
    public void Play_GameOver() { PlaySingle(sfx_GameOver, AS_Base); }
    public void Play_PinataHit() { PlaySingle(sfx_PinataHit, AS_Base); }
    public void Play_Nirvana() { PlaySingle(sfx_Nirvana, AS_Base); }
    public void UI_Confirm() { PlaySingle(ui_confirm, AS_Base); }
    public void UI_Select() { PlaySingle(ui_select, AS_Base); }
    public void UI_Something() { PlaySingle(ui_something, AS_Base); }
    public void Play_LevelDown() { PlaySingle(sfx_levelDown, AS_Base); }
    public void Play_LevelUp() { PlaySingle(sfx_levelUp, AS_Base); }
    #endregion


}
