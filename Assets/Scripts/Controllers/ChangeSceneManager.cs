﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Runtime.InteropServices;

public class ChangeSceneManager : MonoBehaviour {

    public GameDirector gd;
    private string sceneName;
    
    void Start() {
        gd = gameObject.GetComponent<GameDirector>();
        DontDestroyOnLoad(this.gameObject);
    }
    public void ChangeScene(string sceneName)
    {
        this.sceneName = sceneName;
        GameObject.Find("Fader").GetComponent<CameraFade>().fadeOut();
        Invoke("LoadScene", 0.5f);
    }
    public void Quit() {
        Application.Quit(); 
    }

    public void LoadScene()
    {
        gd.LoadScene(sceneName);
        SceneManager.LoadScene(sceneName);
    }

    public void CallBrowserReload()
    {
        GameObject.Find("Fader").GetComponent<CameraFade>().fadeOut();
        Invoke("FadeToReload", 0.5f);
    }
}
