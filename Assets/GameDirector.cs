﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.PostProcessing;
using DG.Tweening;
using UnityEngine;

public class GameDirector : MonoBehaviour {
    StoryTeller storyTeller;
    public AudioSource sfxAudio;
    public AudioClip[] arraySFX;
    public PostProcessingProfile standar;
    public PostProcessingProfile madness;
    public GameObject player;

    void Start() {
        storyTeller = gameObject.GetComponent<StoryTeller>();
    }

    public void LoadScene(string sceneName) {
        if (storyTeller != null) storyTeller.state = StoryTeller.States.Setting;
    }

    public void sfx(int index) {
        sfxAudio.PlayOneShot(arraySFX[index]);
    }

    public void maskOn() {
        GameObject mask = GameObject.FindGameObjectWithTag("mask");
        mask.GetComponent<SpriteRenderer>().DOFade(1f, 0.5f).SetEase(Ease.InOutSine);
    }

    public void redDeath() {
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PostProcessingBehaviour>().profile = madness;
        GameObject death = GameObject.FindGameObjectWithTag("death");
        
        TweenCallback cb = new TweenCallback(() => {
            death.transform.DOMoveY(death.transform.position.y + 1.25f, 2f).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
            death.transform.DOMoveX(death.transform.position.x + 1.75f, 1.5f).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
        });
        death.GetComponent<SpriteRenderer>().DOFade(1f, 1.25f).SetEase(Ease.InOutSine).OnComplete(cb);
    }

    public void relax() {
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PostProcessingBehaviour>().profile = madness;
        GameObject.FindGameObjectWithTag("death").GetComponent<SpriteRenderer>().DOFade(0f, 1.25f).SetEase(Ease.InOutSine);
    }

    public void subir() {
        player = GameObject.FindGameObjectWithTag("player");
        player.transform.DOMoveY(player.transform.position.y + 1.60f, 2f).SetEase(Ease.InOutSine);
        player.transform.DOMoveX(player.transform.position.x + 1.60f, 2f).SetEase(Ease.InOutSine);
    }
}
