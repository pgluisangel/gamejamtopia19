﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Option", menuName = "Scriptables/Option", order = 3)]
public class Option : ScriptableObject {
    public string passage;
    public string sceneName;
}
