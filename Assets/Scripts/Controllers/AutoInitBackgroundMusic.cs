﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoInitBackgroundMusic : MonoBehaviour {

    AudioHelper _audio;

    public bool isMenuMusic;

    // Use this for initialization
    void Start () {
        _audio = GameObject.Find("AudioHelper").GetComponent<AudioHelper>();
        if (!isMenuMusic)
            _audio.PlayGameMusic();
        else
            _audio.PlayMenMusic();
    }
	
	
}
