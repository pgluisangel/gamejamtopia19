﻿using System.Collections;
using UnityEngine;

public class CameraFade : MonoBehaviour {
	
	public float timeToFade;
	public CanvasGroup canvas;
	public bool isStartScene = false;

	void Start() {
		if (isStartScene) StartCoroutine("FadeIn");
	}

	void OnLevelWasLoaded(int scene) {
		StartCoroutine("FadeIn");
	}

	public void fadeOut() {
		StartCoroutine("FadeOut");
	}

	IEnumerator FadeIn() {
		canvas.blocksRaycasts = true;
		yield return new WaitForSeconds(0.25f);
        while(canvas.alpha > 0) {
            canvas.alpha -= Time.deltaTime / timeToFade;
            yield return null;
        }
		canvas.blocksRaycasts = false;
    }

	IEnumerator FadeOut() {
        while(canvas.alpha < 1) {
            canvas.alpha += Time.deltaTime / timeToFade;
            yield return null;
        }
    }
}
